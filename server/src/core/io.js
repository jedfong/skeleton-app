import socketIo from 'socket.io';
import { http } from './http';

export const io = socketIo(http, { path: '/ws' });
