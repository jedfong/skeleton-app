import { Server as server } from 'http';
import { app } from './app';

export const http = server(app);
