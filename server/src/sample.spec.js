import { describe, it } from 'mocha';
import expect from 'expect';

describe('a server test', () => {
  it('should pass', () => {
    const text = 'foo';
    expect(text).toEqual('foo');
  });
});
