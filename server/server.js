import './src/core/polyfill';
import path from 'path';
import { argv } from 'yargs';
import { app, expressStatic, http, logger, io } from './src/core';

const port = parseInt(argv.port, 10) || 3000;
http.listen(port, function listenOnPort() {
  logger.info(`listening on *:${port}`);
});

app.use(expressStatic(path.resolve(__dirname, '../client')));

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../client/index.html'));
});
 
io.on('connection', socket => {
  socket.emit('foo', 'bar');
});
 
