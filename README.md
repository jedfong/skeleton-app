## Synopsis

This project was created for quickly getting started with react/redux/express/socket.io/webpack. I hope it is useful to someone.

## Getting Started

### Installing Dependencies
  Before running the code, dependencies need to be installed.

#### Installing via npm
  `npm install`

#### Installing via yarn
  `yarn install` or just `yarn`

### Running the code

#### NPM Scripts
To run a task, type `npm run <task-name>`. Available tasks are found by running `npm run`. Here are the current list of tasks available:
  * build - build the client/server code for production and output to ./dist
  * dev - run the client/server in dev mode (auto reload and module hot replacement enabled)
  * lint - check for lint errors
  * serve - used after `build` to start up the production code
  * test - run the unit tests

Options can be passed to npm scripts using `--`:<br/>
`npm run test -- --coverage`

#### Runjs
To get around some of the limitations of npm scripts, [runjs](https://www.npmjs.com/package/runjs) is being used under-the-hood. This adds more flexibility and documentation to tasks, but requires `./node_modules/.bin` is added to your path. If setup, runjs can be used like this:

  * `run` - show list of available tasks with documentation
  * `run <task-name> [options]`

For example:<br/>
`run test --coverage`

## Tests

Test files are intended to live along-side the file they are testing.  Naming them *.spec.js will ensure that these can be run via the `npm run test` or `run test` tasks.

## Contributors

If you'd like to contribute, submit a merge request.

## LICENSE

MIT
