import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import { ConnectedRouter as Router } from 'react-router-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

import App from './components/App';
import { history, store, logger } from './core';

injectTapEventPlugin(); // http://stackoverflow.com/a/34015469/988941

logger.info(history);
logger.info(store);

const root = document.getElementById('root');
const render = AppComponent => {
  ReactDOM.render(
    <Provider store={store}>
      <Router history={history}>
        <MuiThemeProvider>
          <AppContainer>
            <AppComponent/>
          </AppContainer>
        </MuiThemeProvider>
      </Router>
    </Provider>,
    root
  );
};

render(App);

if (module.hot) {
  module.hot.accept('./components/App', () => render(require('./components/App')));
}
