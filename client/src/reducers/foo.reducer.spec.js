import { describe, it } from 'mocha';
import expect from 'expect';
import { foo } from './foo.reducer';

describe('a sample test', () => {
  it('should pass', () => {
    expect(foo(undefined, {})).toEqual('foo');
  });
});
