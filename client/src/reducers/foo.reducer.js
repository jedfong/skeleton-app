export const foo = (state = 'foo', { type }) => {
  switch (type) {
    case 'bar':
      return 'bar';
    case 'foo':
      return 'foo';
    default:
      return state;
  }
};
