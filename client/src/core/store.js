import { applyMiddleware, createStore, combineReducers } from 'redux';
import createBrowserHistory from 'history/createBrowserHistory';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import reduxLogger from 'redux-logger';
import config from 'config';
import * as reducers from '../reducers';

const loggerMiddleware = config.reduxLoggerEnabled ? [reduxLogger] : [];

export const history = createBrowserHistory();

export const store = createStore(
  combineReducers({
    ...reducers,
    routing: routerReducer,
  }),
  applyMiddleware(
    thunk,
    ...loggerMiddleware,
    routerMiddleware(history),
  )
);
