import React from 'react';
import Baz from './Baz';

const Foo = () => {
  return (
    <div>
      Foo
      <Baz />
    </div>
  );
};

export default Foo;
