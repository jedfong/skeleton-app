import React from 'react';
import { Route, withRouter } from 'react-router-dom';
import { push } from 'react-router-redux';
import RaisedButton from 'material-ui/RaisedButton';

import { socket, store, logger } from '../core';
import Foo from './Foo';
import Bar from './Bar';
 
if (process.env.NODE_ENV === 'development') {
  logger.info('some dev only code');
}

socket.on('connect', () => {
  socket.on('foo', data => {
    logger.info('incoming message!');
    logger.info(data);
  });
});

const App = () => {
  return (
    <div className="App">
      <RaisedButton label="Foo" onClick={() => store.dispatch(push('/foo'))} />
      <RaisedButton label="Bar" onClick={() => store.dispatch(push('/bar'))} />
      <Route exact path="/" render={() => <Foo />} />
      <Route exact path="/foo" render={() => <Foo/>}/>
      <Route exact path="/bar" render={() => <Bar/>}/>
    </div>
  );
};

export default withRouter(App);
