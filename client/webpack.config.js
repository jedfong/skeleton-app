const resolve = require('path').resolve;

const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const { getIfUtils, removeEmpty } = require('webpack-config-utils');

module.exports = env => {
  const serverPort = env.serverPort || '3001';
  const appConfig = env.config || 'config/config.development.js';

  const { ifDev, ifProd } = getIfUtils(env);

  return {
    entry: {
      app: removeEmpty([
        ifDev('react-hot-loader/patch'),
        ifDev('webpack-dev-server/client?http://localhost:3000'),
        ifDev('webpack/hot/only-dev-server'),
        'babel-polyfill',
        'whatwg-fetch',
        resolve(__dirname, './src/index.js'),
      ]),
    },
    output: {
      path: resolve(__dirname, '../dist/client'),
      pathinfo: true,
      filename: '[name].[hash].js',
      publicPath: '/',
      sourceMapFilename: '[name].[hash].js.map',
      chunkFilename: '[id].chunk.js',
    },
    devtool: env.devtool || 'source-map',
    devServer: {
      stats: {
        assets: false,
        cached: false,
        children: false,
        chunks: false,
        chunkModules: false,
        chunkOrigins: false,
        colors: true,
        errors: true,
        errorDetails: true,
        hash: false,
        modules: false,
        publicPath: false,
        reasons: false,
        source: false,
        timings: true,
        version: false,
        warnings: true,
      },
      proxy: {
        '/ws/*': {
          target: `ws://localhost:${serverPort}`,
          ws: true,
        },
      },
    },
    watchOptions: {
      ignored: /node_modules/,
    },
    resolve: {
      extensions: ['.js', '.json', '.jsx'],
      alias: {
        config: resolve(__dirname, appConfig),
      },
    },
    resolveLoader: {
      modules: [
        resolve(__dirname, '../node_modules'),
      ],
    },
    module: {
      rules: [
        {
          use: [
            {
              loader: 'url-loader',
              query: {
                limit: 10000,
                name: 'static/media/[name].[hash:8].[ext]',
              },
            },
          ],
          exclude: [
            /\.html$/,
            /\.(js|jsx)$/,
            /\.css$/,
            /\.json$/,
            /\.svg$/,
          ],
        },
        {
          test: /\.(js|jsx)$/,
          use: {
            loader: 'eslint-loader',
            options: {
              emitWarning: true,
            },
          },
          exclude: /node_modules/,
          enforce: 'pre',
        },
        {
          test: /\.(js|jsx)$/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                cacheDirectory: true,
              },
            },
          ],
          exclude: /node_modules/,
        },
        {
          test: /\.css$/,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => [autoprefixer({ browsers: ['>1%', 'last 4 versions', 'Firefox ESR', 'not ie < 9'] })],
              },
            },
          ],
        },
        {
          test: /\.json$/,
          use: 'json-loader',
        },
        {
          test: /\.svg$/,
          use: [
            {
              loader: 'file-loader',
              query: {
                name: 'static/media/[name].[hash:8].[ext]',
              },
            },
          ],
        },
      ],
    },
    plugins: removeEmpty([
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(ifProd('production', 'development')),
      }),
      new HtmlWebpackPlugin({
        inject: 'body',
        template: resolve(__dirname, 'src/index.html'),
      }),
      new CaseSensitivePathsPlugin(),
      ifProd(new webpack.optimize.OccurrenceOrderPlugin()),
      ifDev(new webpack.NamedModulesPlugin()),
      ifDev(new webpack.NoEmitOnErrorsPlugin()),
    ]),
    node: {
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
    },
  };
};
