import fs from 'fs';
import { run } from 'runjs';

const task = {};

task.build = () => {
  const args = [
    '--config client/webpack.config.js',
    '-p',
    '--env.prod',
    '--env.config config/config.production.js',
  ];
  run('rimraf dist');
  task.test();
  run('babel server --out-dir dist/server --source-maps');
  run(`webpack ${args.join(' ')}`);
};

task.build.doc = `Build the server and client code and output to the './dist'
   Options:
     None Available
`;

const devClient = (options = {}) => {
  const {
    devtool = 'source-map',
  } = options;
  const args = [
    '--config ./client/webpack.config.js',
    '--inline',
    '--quiet',
    '--progress',
    '--history-api-fallback',
    '--devtool source-map',
    '--debug',
    '--hot',
    '--env.dev',
    `--env.devtool ${devtool}`,
    '--env.config config/config.development.js',
    `--port ${options.port || 3000}`,
    `--env.serverPort ${options['server-port'] || 3001}`,
  ];

  run(`webpack-dev-server ${args.join(' ')}`, { async: true });
};

const devServer = (options = {}) => {
  const {
    port = 3001,
    inspect,
  } = options;
  const args = [
    '--exec babel-node',
    '--watch server',
  ];

  if (inspect || options['debug-brk']) {
    args.push('--inspect');
  }
  options['debug-brk'] && args.push('--debug-brk');

  run(`NODE_ENV=dev nodemon ${args.join(' ')} ./server/server.js --port ${port}`, { async: true });
};

task.dev = (options = {}) => {
  const serverOptions = {
    port: options['server-port'] || 3001,
  };
  if (options['debug-brk']) {
    serverOptions['debug-brk'] = true;
  }
  if (options.inspect) {
    serverOptions.inspect = true;
  }
  
  const clientOptions = {
    'server-port': serverOptions.port,
    port: options.port || 3000,
  };
  
  devServer(serverOptions);
  devClient(clientOptions);
};

task.dev.doc = `Run the client using webpack-dev-server
   Options:
     --debug-brk          - Pause server execution until debuger is attached
     --inspect            - Allow server debugging; enabled when '--debug-brk' is true
     --port=<port>        - WDS port [default: 3000]
     --server-port=<port> - Express port proxied behind WDS [default: 3001]
`;

task.lint = (options = {}) => {
  const args = [];
  options.fix && args.push('--fix');
  !options['no-cache'] && args.push('--cache');
  options['no-color'] && args.push('--no-color');

  run(`eslint ${args.join(' ')} ${options.path || './server ./client'}`);
};

task.lint.doc = `Run eslint against './server and ./client'
   Options:
     --fix      - Fix eslint issues if possible
     --no-cache - Disable eslint caching
     --no-color - Disable eslint color
`;

task.serve = (options = {}) => {
  const {
    port = 3000,
  } = options;
  if (fs.existsSync('dist/server/server.js')) {
    run(`node dist/server/server.js --port ${port}`);
  } else {
    run(`echo build needs to be run first`);
  }
};

task.serve.doc = `Run the previously built code
   Options:
     --port=<port> - Express port [default: 3000]
`;

task.test = ({ coverage } = {}) => {
  run(`${coverage ? 'nyc --exclude **/*.spec.js' : ''} mocha --compilers js:babel-core/register --recursive client/**/*.spec.js server/**/*.spec.js`);
};

task.test.doc = `Run the unit tests
   Options:
     --coverage - Show test coverage
`;

module.exports = task;
